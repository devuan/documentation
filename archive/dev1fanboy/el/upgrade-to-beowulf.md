This document describes how to upgrade to Devuan Beowulf from an existing Devuan system. This should not be used for migrations from Debian.

# Upgrade to Beowulf

First you will need to change your sources.list so that it points to the Beowulf repositories.

`root@devuan:~# editor /etc/apt/sources.list`

Modify the sources.list to look like the one provided. Comment out all other lines.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Update the package lists from the Beowulf repository.

`root@devuan:~# apt-get update`

Devuan Jessie users should now upgrade the Devuan repository keyring, and update the package lists again so packages can be authenticated.

`root@devuan:~# apt-get install devuan-keyring`  
`root@devuan:~# apt-get update`

If xscreensaver is running you should kill it now as it needs to be stopped before it can be upgraded.

`root@devuan:~# killall xscreensaver`

Now you can perform the upgrade.

`root@devuan:~# apt-get dist-upgrade`

In the event of any package failures you should fix the failed packages then start the upgrade again.

`root@devuan:~# apt-get -f install`  
`root@devuan:~# apt-get dist-upgrade`

Users who migrated from ASCII and are using upower will need to downgrade their upower packages to avoid problems like [bug #394](https://bugs.devuan.org/cgi/bugreport.cgi?bug=394).

`root@devuan:~# apt-get install --allow-downgrades upower/beowulf gir1.2-upowerglib/beowulf`

You may want to remove packages that were orphaned by the upgrade process and old package archives.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
