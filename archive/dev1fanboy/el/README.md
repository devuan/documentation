# Εγκατάσταση του Devuan GNU+Linux, αναβαθμίσεις και μινιμαλισμός
Αυτό το κείμενο παρέχει πληροφορίες για την εγκατάσταση, αναβάθμιση και μετανάστευση στο Devuan, και οδηγίες για την επίτευξη ενός πιο λιτού συστήματος. 
Το βίκι είναι διαθέσιμο και σε άλλες γλώσσες, πέρα των Αγγλικών.

## Όλες οι εκδόσεις
[Γενικές πληροφορίες](general-information.md)  
[Εγκατάσταση Devuan](devuan-install.md)  
[Ολική κρυπτογράφηση δίσκου](full-disk-encryption.md)  
[Ρύθμιση δικτύου](network-configuration.md)  
[Devuan δίχως D-Bus](devuan-without-dbus.md)  
[Λογισμικό ελεύθερο από D-Bus](dbus-free-software.md)  
[Ελάχιστη εγκατάσταση xorg](minimal-xorg-install.md)  
[Ελάχιστη εγκατάσταση xfce](minimal-xfce-install.md)

## ASCII
[Μετανάστευση σε ASCII](migrate-to-ascii.md)  
[Αναβάθμιση σε ASCII](upgrade-to-ascii.md)

## Μεταφράσεις

Διατίθενται μεταφράσεις χάρη στην ομάδα βίκι.

[Read in English](../) **|** [Auf Deutsch lesen](../de) **|** [Leer en Español](../es) **|** [Leggi in Italiano](../it) **|** [Leia em Português](../pt)

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
