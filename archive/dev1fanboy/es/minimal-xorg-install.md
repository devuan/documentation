# Instalación mínima de xorg
Este documento describe el procedimiento necesario para realizar una instalación mínima de xorg con una configuración por defecto óptima.

## Instalar los paquetes base de xorg
Para comenzar, abrir una sesión en la terminal como root utilizando la contraseña generada durante la instalación.

Instalar el conjunto mínimo de paquetes necesario para xorg

`root@devuan:~# apt-get install xserver-xorg-video-dummy xserver-xorg-input-void xinit x11-xserver-utils`

Notar que se instala el controlador de entrada void al igual que el controlador de video dummy. Estos paquetes no funcionan como controladores de dispositivos pero permiten que xorg sea instalado sin controladores. De esta manera se evita instalar todos los controladores de dispositivo disponibles, incluyendo aquellos que no serán utilizados. En lugar de eso se instalarán sólo los controladores mínimos necesarios para el funcionamiento del sistema.

## Instalar un controlador de video
A continuación, instalar el controlador de video correspondiente al hardware.

Los controladores de video más comunes son los siguientes:

* xserver-xorg-video-intel (intel)
* xserver-xorg-video-nouveau (nvidia)
* xserver-xorg-video-openchrome (via)
* xserver-xorg-video-radeon (amd)
* xserver-xorg-video-vesa (controlador de video genérico)

Por ejemplo, si se utiliza una tarjeta de video amd se deberá instalar el controlador radeon.

`root@devuan:~# apt-get install xserver-xorg-video-radeon`

En caso de no conocer con certeza el controlador de video necesario, es posible utilizar el controlador vesa, al menos hasta saber más sobre los controladores xorg. Este controlador funciona con todos los dispositivos compatibles con el estándar VESA y es útil como respaldo en el caso de un controlador mal configurado.

`root@devuan:~# apt-get install xserver-xorg-video-vesa`

En el caso de utilizar un dispositivo gráfico no mencionado en el parrafo anterior, se deberá buscar en el repositorio a fin de encontrar el controlador correcto para el mismo.

`root@devuan:~# apt-cache search xserver-xorg-video-.* | pager`

## Instalar los controladores de entrada
Dado que es una dependencia de xserver-xorg no es posible eliminar el controlador evdev. Afortunadamente evdev es un controlador unificado y es adecuado para la mayoría de los dispositivos de entrada. Esto significa que no es necesario seguir estos pasos, a menos que se tengan requerimientos específicos.

En caso de contar con un instalador de ratón y teclado, es posible instalar dichos controladores de manera separada.

`root@devuan:~# apt-get install xserver-xorg-input-mouse xserver-xorg-input-kbd`

Los paneles táctiles (*touchpad*) generalmente requieren instalar el controlador synaptics junto con el controlador de teclado.

`root@devuan:~# apt-get install xserver-xorg-input-synaptics`

Para dispositivos de entrada adicionales, buscar en el repositorio de paquetes.

`root@devuan:~# apt-cache search xserver-xorg-input-.* | pager`

## Instalar algunos extras opcionales

Se recomienda instalar el conjunto básico de fuentes para xorg.

`root@devuan:~# apt-get install xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable`

Generalmente se desea incluir el soporte para mesa opengl cuando se utiliza el controlador de video libre, especialmente para dar soporte a juegos opengl.

`root@devuan:~# apt-get install libgl1-mesa-dri mesa-utils`

ASCII puede necesitar el paquete xserver-xorg-legacy para gestionar permisos sobre xserver. Notar que se trata de un *wrapper* setuid.

`root@devuan:~# apt-get install xserver-xorg-legacy`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

