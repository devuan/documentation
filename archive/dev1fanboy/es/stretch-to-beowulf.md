Estas instrucciones son para migrar exclusivamente desde Debian Stretch. Cuando se migra a Beowulf las instrucciones son específicas para cada versión de Debian desde la que se proviene y deben seguirse al pie de la letra.

# Migrar desde Debian Stretch a Beowulf

Es necesario [configurar la red](network-configuration.md) antes de empezar, de lo contrario se perderá el acceso a la red durante la migración.

Al migrar desde Stretch es necesario instalar sysvinit-core antes de continuar.

`root@debian:~# apt-get install sysvinit-core`

Para prevenir que se cuelgue del sistema luego de rerniciar se necesita remover el paquete libpam-systemd. Esto puede remover el entorno de escritorio, pero será instalado nuevamente más adelante.

`root@debian:~# apt-get purge libpam-systemd`

Cambiar el archivo sources.list para que apunte a los repositorios de Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificar el contenido para que quede como se muestra a continuación. Comentar las líneas restantes.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Actualizar la lista de paquetes desde los repositorios de Beowulf.

`root@debian:~# apt-get update`

Instalar el *keyring* de Devuan para que los paquetes puedan ser autenticados.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Actualizar las listas de paquetes nuevamente para que los paquetes sean autenticados de ahora en adelante.

`root@debian:~# apt-get update`

Se necesita actualizar el paquete libtinfo para prevenir errores.

`root@debian:~# apt-get install libtinfo6`

Actualizar los paquetes a su última versión. Notar que esto no completa la migración.

`root@debian:~# apt-get upgrade`

Luego es necesario reiniciar el sistema para cambiar sysvinit a PID 1.

`root@debian:~# reboot`

El último paso antes de la migración consiste en cambiar a eudev.

`root@debian:~# apt-get install eudev`

Ahora es posible ejecutar la migración adecuadamente. Se recomienda ejecutar full-upgrade para migraciones desde Stretch.

`root@debian:~# apt-get full-upgrade`

Si algo se rompe será necesario arreglarlo y correr full-upgrade nuevamente.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get full-upgrade`

Si no se cuenta con un entorno de escritorio es posible instalar uno. Por defecto Devuan utiliza XFCE.

`root@devuan:~# apt-get install task-xfce-desktop`

O es posible instalar Gnome si se desea continuar utilizándolo.

`root@devuan:~# apt-get install task-gnome-desktop`

En este punto es seguro eliminar systemd.

`root@devuan:~# apt-get purge systemd`

Finalmente se recomienda eliminar paquetes que hayan quedado huérfanos durante el proceso de migración, junto con archivos de paquetes obsoletos dejados por la instalación de Debian.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

