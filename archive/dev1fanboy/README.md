# Devuan GNU/Linux installation, upgrades and minimalism
This documentation provides information on installing, upgrading and migrating to Devuan and how to reduce the system to a more minimal state. The wiki is also available in languages other than English.

## All releases
[General information](en/general-information.md)  
[Installing Devuan](en/devuan-install.md)  
[Full disk encryption](en/full-disk-encryption.md)  
[Network configuration](en/network-configuration.md)  
[Devuan without D-Bus](en/devuan-without-dbus.md)  
[D-Bus free software](en/dbus-free-software.md)  
[Minimal xorg install](en/minimal-xorg-install.md)  
[Minimal XFCE install](en/minimal-xfce-install.md)  

## ASCII
[Migrate to ASCII](en/migrate-to-ascii.md)  
[Upgrade to ASCII](en/upgrade-to-ascii.md)

## Beowulf
[Upgrade Devuan to Beowulf](en/upgrade-to-beowulf.md)  
[Migrate from Debian Buster](en/buster-to-beowulf.md)  
[Migrate from Debian Stretch](en/stretch-to-beowulf.md)  
[Migrate from Debian Jessie](en/jessie-to-beowulf.md)

## Translations

Translations have been made available thanks to the wiki team.

[Auf Deutsch lesen](de) **|** [Διαβάστε στα Ελληνικά](el) **|** [Leer en español](es) **|** [Leggi in Italiano](it) **|** [Leia em Português](pt)

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
