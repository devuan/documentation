# Devuan senza D-Bus
Questa guida descrive come rimuovere D-Bus da Devuan compresa la sostituzione dell'interfaccia grafica con una più leggera, la scelta di un browser internet e una soluzione alternativa all'auto-mounting offerto da D-Bus.

## Scelta di un'interfaccia grafica
A differenza degli ambienti desktop completi quasi tutte le interfacce grafiche non hanno come dipendenza D-Bus, quindi sarebbe saggio installarne una.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Installazione e configurazione di Fluxbox
Descriveremo qui Fluxbox in quanto semplice e facile da usare.

`root@devuan:~# apt-get install fluxbox`

Selezionate Fluxbox come interfaccia grafica di default per il vostro utente quando si lancia lo script startx.

`user@devuan:~@ echo "exec fluxbox" >> .xinitrc`

Quindi usate lo script startx per lanciare Fluxbox.

`user@devuan:~@ startx`

Un'alternativa valida per un gestore grafico di login è WDM.

`root@devuan:~# apt-get install wdm`

## Scelta di un browser internet
I browser internet che non hanno come dipendenza una componente di dbus sono pochi, qualcuno vi soddisferà e qualcun'altro no.

* midori
* surf
* links2
* dillo
* lynx

Sceglieremo il ben noto Midori in quanto è il più ricco di funzionalità

`root@devuan:~# apt-get install midori`

## Rimozione di D-Bus da Devuan
Passiamo ora alla rimozione di dbus da Devuan.

`root@devuan:~# apt-get purge dbus`

Rimuoviamo anche tutti i pachetti "orfanizzati" dalla rimozione di dbus.

`root@devuan:~# apt-get autoremove --purge`

## Una semplice alternativa all'auto-mounting
In assenza di D-Bus la maggioranza dei gestori dei file non disporrà della funzione di auto-mounting, poichè questa funzione richiede D-Bus e metodi di mounting più semplici non sono sempre presenti. Andremo a crearci un punto di mount da noi, di modo che i gestori dei file migliori possano montare i volumi in pochi click.

### Creazione manuale dei punti di mount
Create una directory per il nuovo punto di mount.

`root@devuan:~# mkdir /media/usb0`

Fate un copia del file /etc/fstab prima di modificarlo.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Ora modifichiamo fstab.

`root@devuan:~# editor /etc/fstab`

Bisogna aggiungere un punto di mount per un disco USB alla fine del fstab. Assicuratevi di selezionare l'opzione `user` così che anche gli utenti non-root possano montare l'unità.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

I parametri delle periferiche variano a seconda della vostra configurazione. Potrete scoprirli inserendo il disco USB e servendovi del comando `lsblk`.

Inserite un disco USB e verificate che la vostra configurazione funzioni correttamente.

`user@devuan:~$ mount -v /media/usb0`  
`user@devuan:~$ umount -v /media/usb0`

### Scelta del gestore dei file
Se volete un gestore dei file grafico in grado di montare e smontare i dischi basandosi sul file fstab potete usare Xfe.

`root@devuan:~# apt-get install xfe`

Un'interessante alternativa minimalista tra i gestori dei file è rappresentata da Midnight Commander, basato su ncurses.

`root@devuan:~# apt-get install mc`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
