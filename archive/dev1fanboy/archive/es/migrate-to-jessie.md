# Migrar a Devuan Jessie
Este documento explica cómo migrar a Devuan Jessie desde Debian.

## Realizar la migración
Para migrar a Devuan Jessie simplemente se debe editar la lista de *mirrors* y apuntar a los repositorios de Devuan como fuente de paquetes.

`root@debian:~# editor /etc/apt/sources.list`

Cambiar a los *mirrors* de Devuan, comentando los de Debian preexistentes.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Antes de comenzar la instalación de paquetes de Devuan es necesario actualizar los archivos índice de paquetes.

`root@debian:~# apt-get update`

El *keyring* de Devuan es requerido para autenticar los paquetes.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Con el *keyring* de Devuan instalado, es necesario actualizar nuevamente los archivos índice de paquetes, a fin de que sean autenticados a partir de ahora.

`root@debian:~# apt-get update`

Ahora es posible migrar a Devuan. Seleccionar slim como gestor de pantalla por defecto en caso de que se requiera.

`root@debian:~# apt-get dist-upgrade`

Para eliminar a systemd como PID 1, se requiere un reinicio por única vez.

`root@devuan:~# reboot`

## Tareas post-migración
Si se utilizaba GNOME bajo Debian antes de la migración, se recomienda cambiar al gestor de sesiones a startxfce4.

`root@devuan:~# update-alternatives --config x-session-manager`

Los componentes de systemd deberían ahora ser removidos del sistema.

`root@devuan:~# apt-get purge systemd systemd-shim`

Si no se utiliza D-Bus, es posible eliminar libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Eliminar cualquier paquete huérfano que haya quedado de la instalación previa de Debian.

`root@devuan:~# apt-get autoremove --purge`

A su vez es un buen momento para eliminar todos los archivos de paquetes obsoletos dejados por el anterior sistema Debian.

`root@devuan:~# apt-get autoclean`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

