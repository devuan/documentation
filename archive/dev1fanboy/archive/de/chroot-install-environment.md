Dies ist ein bisschen experimentelle Dokumentation, benötigt einige Verbesserungen an den chroot-Befehlen.

# Erstellen einer Chrootable-Installationsumgebung

Der Zweck dieses Dokuments besteht darin, zu zeigen, wie eine von Ihrem laufenden System getrennte Installation von Devuan erstellt wird. Dies kann für die Virtualisierung nützlich sein oder um bestimmte Pakete zu installieren, ohne das laufende System zu verändern, was für Testzwecke nützlich ist.

Zu Illustrationszwecken beziehen wir uns bei der Installation der Chroot-Umgebung auf die Ceres-Version (Unstable). Falls Sie eine andere Version wünschen, ersetzen Sie 'ceres' den gewünschten Versionsnamen.

##  Erstellen der Chroot-Installation

Installieren Sie das deboostrap Paket, das wir für die Installation benötigen.

`root@devuan:~# apt-get install debootstrap`

Sie müssen ein chrootable System irgendwo platzieren, also sollten Sie irgendwo ein Verzeichnis dafür anlegen.

`root@devuan:~# mkdir /mnt/ceres`

Schließen Sie nun den chroot-Installationsprozess mit debootstrap ab.

`root@devuan:~# debootstrap ceres http://pkgmaster.devuan.org/merged /mnt/ceres`

## Chrooten der Installation

Bei Verwendung der Chroot-Umgebung müssen Sie manchmal sicherstellen, dass `dev`,` proc` und `sys` dort gemountet sind.

`root@devuan:~# mount -o bind /dev /mnt/ceres/dev`

`root@devuan:~# mount -o bind /sys /mnt/ceres/sys`

`root@devuan:~# mount -t proc proc /mnt/ceres/proc`

Chroot die Umgebung, um davon Gebrauch zu machen.

`root@devuan:~# chroot /mnt/ceres /bin/bash`

## Beenden der Chroot-Umgebung

Wenn Sie fertig sind, sollten Sie sicherstellen, dass Sie die Chroot-Umgebung sauber verlassen.

`root@chroot:~# exit`

`root@devuan:~# umount /mnt/ceres/dev`

`root@devuan:~# umount /mnt/ceres/sys`

`root@devuan:~# umount /mnt/ceres/proc`

---

<sub>**Diese Arbeit wurde unter der Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de)] Lizenz veröffentlicht. Alle Marken sind Eigentum ihrer jeweiligen Inhaber. Diese Arbeit wird "WIE BESEHEN" zur Verfügung gestellt und kommt mit KEINERLEI Garantie.**</sub>