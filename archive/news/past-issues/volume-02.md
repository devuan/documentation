# [Devuan Weekly News](..)

[Archives](home) for __volume 02__ (12015 [HE](../why-he "Holocene Era"))

## January

- 06 Issue VI was not released.
- 12 [Issue VII](volume-02/issue-007)
- 20 [Issue VIII](volume-02/issue-008)
- 27 [Issue IX](volume-02/issue-009)

## February

- 03 [Issue X](volume-02/issue-010)
- 11 [Issue XI](volume-02/issue-011)
- 18 [Issue XII](volume-02/issue-012)
- 24 [Issue XIII](volume-02/issue-013)

## March

- 03 [Issue XIV](volume-02/issue-014)
- 10 Issue XV was not released.
- 17 Issue XVI was not released.
- 31 [Issue XVII](volume-02/issue-017)
- Issue XVIII was not released.

## April

- Issue XIX was not released.
- Issue XX was not released.
- Issue XXI was not released.
- Issue XXII was not released.

## May

- 5 Issue XXIII was not released.
- 12 [Issue XXIV](volume-02/issue-024)
- 19 [Issue XXV](volume-02/issue-025)
- 26 Issue XXVI was not released.

## June

- 2 [Issue XXVII](volume-02/issue-028)
- Issue XXIX was not released.
- Issue XXX was not released.
- Issue XXXI was not released.

## July-December

- No issue was released during this period (Issues XXXII to LVI)



[previous Volume](volume-01)
[next volume](volume-03)
