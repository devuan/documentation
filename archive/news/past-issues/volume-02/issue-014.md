# Devuan Weekly News Issue XIV

__Volume 002, Week 9, Devuan Week 14__

Released 03/03/12015 [HE](why-he)

https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-02/issue-014

## Editorial

It's hard to believe it's winter when you have to mop the sweat out of
your keyboard, but the intensity of this week's conversations, and
@golinux's [penguins][0] made thinking about cold easier.  Cold reigns
in deep space as well and Devuan users will appreciate the identity
moving away from toyland: although Debian Jessie refers to an
adventurous toy cowgirl with an attitude, Devuan's Jessie refers to a
place no toy has ever gone before.  Exit the naughty `Sid` brat,
welcome `Ceres`, largest object in the asteroid belt, and the first
minor planet discovered in the 19th Century.  That's right, [Devuan
release codenames][1] will be named after minor planets of our solar
system.  As far as visual identity goes, and although the logo still
consumes a significant bit of attention, it won't be revealed before
the code: part of the distro's publishing policy is to deliver working
code before a shiny image.  Welcome to Issue XIV of the DWN, cooked
_al dente_ by your interim editor, @hellekin, with the invaluable help
of @golinux and @joerg_rw.

## Last Week in Devuan

### [Debian Problems with Jessie][2]

T.J.Duchene advises not to bring "Debian mud" to the list: "Devuan
does not need to justify its own existence." Here comes the largest
thread this week: a swashbuckling introspective and speculative bubble
visiting what's wrong with collective creation of software. (It all
started with _Simple Backgrounds_, go figure.)

### [Three Important UI Features][3]

Last week Jonathan Wilkes introduced the idea of improving the
features for the default devuan desktop.  Wolfgang Pirker proposes two
solutions for the "find apps as you type" #2 feature.  Feature #3
"menu on the Super key" can be addressed with `dmenu`.  A consensual
voice raises in favor of Xfce as the default desktop environment (DE),
which comes with its own implementation equivalent to what dmenu
provides.

In passing, for your hacker jeopardy, here's a funky compiler flag in
`Enlightenment` that will probably break the layout of many screen
applications displaying this:
`--enable-i-really-know-what-i-am-doing-and-that-this-will-probably-break-things-and-i-will-fix-them-myself-and-send-patches-aba`.

### [Xfce Desktop Environment in Devuan][4]

As `Xfce 4.12` was released this week on the 28th of February, it was
also chosen to become the default DE in Devuan, which makes it the
first _non-systemd_ difference between cowgirl Jessie and planet
Jessie.  David Harrison talked with the Xfce team, and @jaromil
confirms existing coordination and good terms between Devuan and Xfce.

### [Logind Alternative][5]

Oz Tiram introduces the `ConsoleKit2` fork that does not depend on
`systemd-logind`.  Svante Signell offers to package it for Devuan.
Dima Krasner reminds that "ConsoleKit2 was already packaged by Max,
but we don't need it in the Jessie cycle because the logind dependency
was dropped from all packages."

### [Simple Backgrounds][6]

Hendrik Boom is concerned not to delay the first release for designing
the project's visual identity (and he's right). As @jaromil puts it:
"Devuan is sugar-free and doesn't makes your computer fat :^)" See the
editorial.

### More of Devuan Logo

Discussions abound regarding the potential Devuan logo.  Linuxito
announces a [graphical version of the survey][7], warning that it's
informative, and Anto notes that a logo without matching text "would
look strange".  In [another thread][8], Hendrik Boom suggests using
the Milky Way as the basis for the logo (that was before the release
code names were announced, but it was in the air).  Tzu-Pei Chen shows
that a single-arm spiral galaxy exists, which Hendrik qualifies as
"debian galaxy", leaving the interpretation of the comment's depth to
the astute reader.  Neo Futur engages in the [philosophy of KISS][9].

### The Valentine <3 Pre-Release Support

More people come with feedback over the Valentine Pre-Release ISO.
Please [report any issue][10] with this release so the team can ensure
these are gone in the next batch.  John Morris reports [quite a few
issues][11], some of them coming from Debian Installer itself.  His
verdict: "most problems were fixable and process 1 is init
so.... Winning!"

Hendrik Boom plans [to install Valentine pre-alpha on real
hardware][12].  So far he got the hardware to boot from USB using the
`isohybrid` method as `dd` would not work, but got stuck with manual
partitioning: the disk seems not to offer a safe option to [preserve
existing partitions][13].  He also mentions a long-standing but minor
bug in debootstrap that prevents Debian from offering multiple inits.
Svante Signell narrows down the issue to Debian bug #668001, and a
maintainer refusing to include the patch in Jessie. Hint: that patch
_will_ be in Devuan Jessie.  Hendrik also had issues with
partitioning, and suspicions rose against systemd inside the
installer.  Paranoia?  Adam Borowski suspects something else went
wrong as he was able to use existing partitions like in Wheezy.
Richard and Hendrik promise to investigate further.

In the end, we've seen our [first community support request][14]
fulfilled.  Congratulations Steve Litt for asking, and Gravis for
solving!

### [Looking for advices in preparation to switch from Debian to Devuan][15]

Anto follows up on last week's VPS experiment to run Debian
Wheezy/Jessie without systemd appearing in the logs, in preparation of
switching to Devuan...

### [What if systemd infects the kernel?][16]

@golinux smells the elephant in the room.  Jude claims it's unlikely,
and that Kay Sievers' `kdbus` code was rejected because he didn't use
the documented interface to request a firmware driver, causing `udev`
to hang.  BTW, `dbus` and `kdbus` only share 4 letters of their name.

### [Circumvention Tech Festival][17]

Influenced by his presence at the [CTF][18], our dear
editor-in-chief @envite asks: "can Devuan be a security and
privacy-aware distro"?  @jaromil dares "to say that Devuan is more
security-aware than Debian". He goes on to explain why, and mentions
in passing "doing our best to contain the attitude of GNOME and other
DE developments which are spawning daemons like there is no tomorrow".

### [Easy Forkability][19]

A follow-up on the question: "Are there aspects of the existing
structure of Debian that made it more difficult to fork?", where
design-by-committee meets the wisdom-of-crowds.  Indeed, in an
adhocracy, communication is key.  Or as Hendrik puts it "The
interaction structure of the crowd is a key element."

### Devuan Documentation

Beginning of [Devuan Dmenu Howto][20], by Steve Litt who inaugurates
the [Devuan documentation][21] group and project.

### [Disnovation][22]

@jaromil introduces a blog from Regine Debatty about "disnovation: an
inquiry into the mechanics and rethoric of innovation".
P. T. Zoltowski remarks that "if you see an analogy with some init
system here, it's because there's one!", claiming that nature uses DNA
replication where the fascist mind replaces the worse-old with the
better-new. Martijn Dekkers slips in some illustration of iDeal
addiction. Bottom line: "the more diversity the better, for everyone".

### [Combatting Revisionist History][23]

@golinux describes an "excellent analysis of the systemd debacle" and
a "required reading".  So we won't spoil it.  Nevertheless,
T.J. Duchene promptly reacted to "respectfully disagree", claiming
"the analysis to be very biased".  Steve Litt responds by saying that
"the vast majority of this mailing list, whose project was created in
order to choose one's init system without trashing the entire OS"
shares the author's bias.  Neo Futur agrees that systemd "does so much
more than just the init system", leading the GNU/Linux OS to turn into
"a redhat-glibc-systemd-linux".  An interesting discussion to wrap
one's mind around features that systemd brings, that could be useful
to implement in a modular way to "perform the same task without
becoming The Blob." (Gravis)

### [free and open world][24]

Hendrik Bloom invites to discuss Clarke's remark of last week: "I
imagine there will be a lot of back and forth sharing as time goes on
that is the nature of the free and open world."

One remark: when you say "free and open", can you imagine saying "free
and close" (sounds like a date), or "prisoner and open"? (Call
Torquemada!)

### [Beware The Red Hat Octopus][25]

Steven W. Scott warns against Red Hat's way, comparing systemd with
Microsoft's failed attempt to supplant Sun's Java.  (Is there a Godwin
point for mentioning M$?)  He offers to chainsaw the octopus'
tentacles.

### [Why is there only one dbus?][26]

Godefridus Daalmans asks in a self-confessed long email (but full of
interesting thoughts).  Nate Bargmann replies shortly, distilling some
mostly *NIX wisdom: "Most likely because it has mostly been restrained
to its problem domain and no one has been troubled enough by it to
reimplement a replacement."  Jude Nelson suggests that if
"inter-process communication could be done all over again from
scratch" it would probably take the form of userspace API filesystems.

## Missing Bits

As the activity on the list intensified and the topics covered
diversified, we decided to keep some out of the Weekly News.  Here's
what we've skipped that you may want to check for yourself:

+ [Troubleshooting][27]
+ [KDE systemd lock-in][28]
+ [A New Video and Text I came across][29]
+ [wicd & wpasupplicant][30]
+ [btrfs works fine, Lennart has no idea what he is talking about][31]
+ [Init Freedom Badges][32]

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Read you next week!

Devuan Weekly News is made by your peers: you're [welcome to contribute][wiki]!

+ @envite (editor at large)
+ @hellekin (interim editor)
+ @golinux (word wrangler)
+ Joerg @joerg_rw Reisenweber (document scrutinizer)


[0]: http://www.saynotogmos.org/ss/penguins/trio.png "@golinux penguins"
[1]: https://git.devuan.org/devuan/devuan-project/wikis/devuan-codenames "Devuan release codenames"
[2]: https://lists.dyne.org/lurker/message/20150227.201815.f934f01f.en.html "Debian Problems with Jessie"
[3]: https://lists.dyne.org/lurker/message/20150224.133648.c5d53629.en.html "three important UI features"
[4]: https://lists.dyne.org/lurker/message/20150224.195105.abb6d702.en.html "Xfce response"
[5]: https://lists.dyne.org/lurker/message/20150224.214900.48c9d626.en.html "Logind alternative (spolier: Consolekit fork)"
[6]: https://lists.dyne.org/lurker/message/20150226.170458.4ac4785c.en.html "Simple Backgrounds"
[7]: https://lists.dyne.org/lurker/message/20150225.161041.bfc73c54.en.html "Devuan Logo Survey"
[8]: https://lists.dyne.org/lurker/message/20150224.122839.72ac21cc.en.html "logo again"
[9]: https://lists.dyne.org/lurker/message/20150224.084456.df74b744.en.html "[philosophy] KISS, roots linux and the logo"
[10]: https://git.devuan.org/devuan/devuan-project/issues "Issue tracker for Datalove Pre-Release milestone"
[11]: https://lists.dyne.org/lurker/message/20150226.030100.3b4989f9.en.html "Kicking the tires on Valentine release"
[12]: https://lists.dyne.org/lurker/message/20150223.212412.c6bb71b4.en.html "plan to install valentine pre-alpha on real hardware."
[13]: https://lists.dyne.org/lurker/message/20150301.174447.e291a4c2.en.html "No way to use a prepartioned disk?"
[14]: https://lists.dyne.org/lurker/message/20150301.163821.c9b5b507.en.html "Us as tech support"
[15]: https://lists.dyne.org/lurker/message/20150301.084714.4512f741.en.html "Looking for advices in preparation to switch from Debian to Devuan"
[16]: https://lists.dyne.org/lurker/message/20150228.070546.023c236c.en.html "What if systemd infects the kernel?"
[17]: https://lists.dyne.org/lurker/message/20150301.182010.e5ddfe3b.en.html "Circumvention Tech Festival"
[18]: https://openitp.org/festival/about.html "CTF website"
[19]: https://lists.dyne.org/lurker/message/20150227.190517.6001e8e9.en.html "Easy Forkability"
[20]: https://lists.dyne.org/lurker/message/20150302.050744.48e28d7c.en.html "Beginning of Devuan Dmenu Howto"
[21]: https://lists.dyne.org/lurker/message/20150301.222647.fc2b8558.en.html "Doc Devuan"
[22]: https://lists.dyne.org/lurker/message/20150228.104616.6b097a4d.en.html "Disnovation: an inquiry into the mechanics and rhetoric of innovation"
[23]: https://lists.dyne.org/lurker/message/20150225.211109.1a574293.en.html "Combatting revisionist history"
[24]: https://lists.dyne.org/lurker/message/20150218.143334.5a1bfc70.en.html "free and open world"
[25]: https://lists.dyne.org/lurker/message/20150224.233014.d436be37.en.html "without-systemd"
[26]: https://lists.dyne.org/lurker/message/20150225.160242.f0adbafc.en.html "idea for discussion: why 1 dbus [long e-mail]"
[27]: https://lists.dyne.org/lurker/message/20150223.224634.898ad466.en.html "It may be only one file, but it does point to the bigger problem!"
[28]: https://lists.dyne.org/lurker/message/20150224.141634.914d557e.en.html "KDE systemd lock-in"
[29]: https://lists.dyne.org/lurker/message/20150226.034512.35133b4e.en.html "A New Video and Text I came across."
[30]: https://lists.dyne.org/lurker/message/20150224.203530.87e75a06.en.html "wicd & wpasupplicant"
[31]: https://lists.dyne.org/lurker/message/20150227.161511.cb3a7c50.en.html "btrfs repair works fine, Lennart has no idea what he is talking about - was OT - It may be only one file, but it does point to the bigger problem!"
[32]: https://lists.dyne.org/lurker/message/20150227.192106.bb8ec903.en.html "Init Freedom Badges"


[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
[why-he]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/Why-HE
