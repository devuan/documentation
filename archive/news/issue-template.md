# Devuan News Issue $ROMAN

__Volume $VOL, Week $WEEK, Devuan Week $DWN__

Released $DATE

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-$VOL/issue-$DN

## Editorial

(Enter the editorial here and sign it. Currently the prerogative of {put
your name here})

## Comings and Goings

(OPTIONAL: any life events that may affect the project, e.g., an
upcoming get-together, vacations of a developer, expected travels and
delays...)

## Lately in Devuan

(Each thread)

### [Some Thread on the List][1]

(The title should link to the relevant thread, starting on the first
post for this week.)

X said .... Y ensured that...

### [Another Thread][2]

...

## Any Extra Section

(OPTIONAL: for some special announcement)

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!


[1]: https://lists.dyne.org/lurker/message/20150113.010327.7bf78a42.en.html "Some Thread in the List"
[2]: URL "section title"
...

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan
News"
