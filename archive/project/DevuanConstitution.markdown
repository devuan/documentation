# Devuan Constitution
## Version 0 (proposed draft)
Version 1 of the devuan constitution will be discussed with all contributors, but final decision on adopting it will be done by the VUA group that originally started the devuan fork.

* [1. Introduction](#1-introduction)
* [2. Project goals](#2-project-goals)
* [3. Social Contract](#3-social-contract)
* [4. Decision-making bodies and individuals](#4-decision-making-bodies-and-individuals)
* [5. Individual developers and contributors](#5-individual-developers-and-contributors)
* [6. Users](#5-users)
* [7. The VUA group](#7-the-vua-group)
* [8. Project leaders](#8-project-leaders)
* [9. Free Software Guidelines](#9-devuan-free-software-guidelines)

## 1. Introduction

The Devuan project is an association of individuals who have made common cause to create a free operating system.

This document describes the organizational structure for formal decision-making in the Project, the project primary goals, and the promises to the Devuan users and contributors in the form of a social contract.

## 2. Project goals 
### 2.1 build an universal UNIX like OS

Devuan is a free operating system (OS) for any computer system, from home desktops to servers, from embedded devices of any sort to big complex systems. Our main goal is to provide an universal OS that can be as easily as we can adapted to any sort of use case and environment, following the UNIX principles as guidelines.

### 2.2 protect freedom of choice of users and developers

An universal OS must protect freedom of choice, trying to provide all the alternatives to adapt to any use case, from a common home user to a developer and/or a sysadmin, and avoiding to force minoritarian users to work in the way the majority follow, as any minority has usually good reasons for doing thing in different way.

### 2.3 build a light and peacefull space for contributors



### 2.4 improve relationship with both upstream and downstream
### 2.5 provide a rock solid, stable and innovation oriented base OS framework for derivates, professionals, entusiast
## 3. Social Contract
### 3.1 Devuan will remain 100% free

We provide the guidelines that we use to determine if a work is free in this document as in the section [9. Free Software Guidelines](#9-devuan-free-software-guidelines). We promise that the Devuan system and all its components will be free according to these guidelines. We will support people who create or use both free and non-free works on Devuan. We will never make the system require the use of a non-free component.

### 3.2 We will give back to the free software community

When we write new components of the Devuan system, we will license them in a manner consistent with the [Free Software Guidelines](#9-devuan-free-software-guidelines). We will make the best system we can, so that free works will be widely distributed and used. We will communicate things such as bug fixes, improvements and user requests to the upstream authors of works included in our system.

### 3.3 We will not hide problems

We will keep our entire bug report database open for public view at all times. Reports that people file online will promptly become visible to others.

### 3.4 Our priorities are our users and free software

We will be guided by the needs of our users and the free software community. We will place their interests first in our priorities. We will support the needs of our users for operation in many different kinds of computing environments. We will not object to non-free works that are intended to be used on Devuan systems, or attempt to charge a fee to people who create or use such works. We will allow others to create distributions containing both the Devuan system and other works, without any fee from us. In furtherance of these goals, we will provide an integrated system of high-quality materials with no legal restrictions that would prevent such uses of the system.

### 3.5 Works that do not meet our free software standards

We acknowledge that some of our users require the use of works that do not conform to the [Free Software Guidelines](#9-devuan-free-software-guidelines). We have created contrib and non-free areas in our archive for these works. The packages in these areas are not part of the Devuan system, although they have been configured for use with Devuan. We encourage CD manufacturers to read the licenses of the packages in these areas and determine if they can distribute the packages on their CDs. Thus, although non-free works are not a part of Devuan, we support their use and provide infrastructure for non-free packages (such as our bug tracking system and mailing lists).

## 4. Decision-making bodies and individuals
## 5. Individual developers and contributors
## 6. Users
## 7. the VUA group
## 8. Project leaders
## 9. Devuan Free Software Guidelines

### 9.1 Free Redistribution

The license of a Devuan component may not restrict any party from selling or giving away the software as a component of an aggregate software distribution containing programs from several different sources. The license may not require a royalty or other fee for such sale.

### 9.2 Source Code

The program must include source code, and must allow distribution in source code as well as compiled form.

### 9.3 Derived Works

The license must allow modifications and derived works, and must allow them to be distributed under the same terms as the license of the original software.

### 9.4 Integrity of The Author's Source Code

The license may restrict source-code from being distributed in modified form only if the license allows the distribution of patch files with the source code for the purpose of modifying the program at build time. The license must explicitly permit distribution of software built from modified source code. The license may require derived works to carry a different name or version number from the original software. (This is a compromise. The Devuan group encourages all authors not to restrict any files, source or binary, from being modified.)

### 9.5 No Discrimination Against Persons or Groups

The license must not discriminate against any person or group of persons.

### 9.6 No Discrimination Against Fields of Endeavor

The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research.

### 9.7 Distribution of License

The rights attached to the program must apply to all to whom the program is redistributed without the need for execution of an additional license by those parties.

### 9.8 License Must Not Be Specific to Devuan

The rights attached to the program must not depend on the program's being part of a Devuan system. If the program is extracted from Devuan and used or distributed without Devuan but otherwise within the terms of the program's license, all parties to whom the program is redistributed should have the same rights as those that are granted in conjunction with the Devuan system.

### 9.9 License Must Not Contaminate Other Software

The license must not place restrictions on other software that is distributed along with the licensed software. For example, the license must not insist that all other programs distributed on the same medium must be free software.

### 9.10 Software must not encourage lock-in

According to the UNIX philosophy and KISS principles, core software in Devuan should be "as simple as possible, but not simpler" and hopefully follow the "do one thing and do it well" where feasible, with the only exception of the OS kernel. Core software included in main distribution must try avoid to require other independent parts of the system to function, trying to provide best portability, POSIX compliance and standards adherence. 

### 9.11 Example Licenses

The [GPL](http://www.gnu.org/copyleft/gpl.html), [BSD](https://www.debian.org/misc/bsd.license), and [Artistic](http://perldoc.perl.org/perlartistic.html) licenses are examples of licenses that we consider free. A more exaustive list of free licenses can be found here: [GNU list of Free Licenses](http://www.gnu.org/licenses/license-list.html)

### 9.12 Guidelines credits

The software guidelines are based on the original ["The Debian Free Software Guidelines"]( http://www.debian.org/social_contract) with few addition for the Devuan project.