We will try to replicate as closest possible the debian infrastructure, but changes are well accepted if they improve and modernize what debian have.

The server list isn't yet disclosed for security purpose, and only core members can access the complete list in the private core project right now.

Anyway, we can give some details:

The software used for the master repository is dak, the default one used also by debian. A couple of servers are actually serving for this purpose, one for dak itself and one for postgres.

For sources we use gitlab (this server, uh!), and jenkins for continouos integration. Actually there are 9 build servers and a jenkins master.

There are also other machines, for web, wiki, mailing lists and so on. All the machines are offered to the project for free from VUAs or early contributors.