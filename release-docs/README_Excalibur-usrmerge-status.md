# Navigating the usrmerge transition

  1. The transition to usrmerge mandated by Debian is currently in progress and may result in atypical instability and breakage during the current testing cycle Excalibur/Trixie.

  2. The safest way to migrate from Daedalus/stable to Excalibur/testing is to install the `usrmerge` package ***before*** proceeding with the normal upgrade steps.

  3. Note that this is not reversible!

  4. Be aware that during the transition there may be issues with encrypted partitions, raid, firmware etc. because merged and non-yet-merged dependencies might be out of sync.

  5. Make sure you have viable backup(s) before going where Linux users have never gone before!

  6. This document will be updated if new issues or solutions are found.
