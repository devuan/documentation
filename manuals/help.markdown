# How to Get and Offer Help with Devuan

## RTFM

Read the [fine manuals](/devuan-doc/manuals) offered by [Doc'Devuan](/devuan-doc).

As with other free software, programs in __devuan__ come with an inline manual accessible with the `man` command, or using the GNU `info` documentation system.  Our gitlab wikis also contain information about all kinds of things related to __devuan__.

You can also consult [Debian documentation](https://www.debian.org/doc/).  As __devuan__ is a [Debian fork](https://debianfork.org/), most of the Debian documentation remains valid.

## Community Support

We're still a young community, so we do not have specific channels for documentation.  Our general communication channels are the IRC channels [#debianfork](http://webchat.freenode.net/?randomnick=1&channels=%23debianfork&uio=MTA9dHJ1ZSYxMT0yNzc39) for general discussion, and [#devuan](ircs://irc.freenode.net/devuan) for developer discussion.

You're also invited to [participate in the mailing list](https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng).

## Contributing to Devuan Documentation

Anyone can [join the devuan-doc](/devuan-doc) group.  You're welcome to do so. 

## Devuan specific documentation

 * [Devuan project](https://git.devuan.org/devuan/devuan-project/wikis/home)
 * [Devuan doc project](https://git.devuan.org/devuan/devuan-documentation/wikis/home)
 * [Devuan Maintainers](https://git.devuan.org/devuan/devuan-maintainers)
